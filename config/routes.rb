
Rails.application.routes.draw do
  namespace :admin do
      resources :users
      resources :client_registrations
      resources :customisations
      resources :enquiries
      resources :enquiry_details_pages
      resources :homes
      resources :operations_teams
      resources :orders
      resources :supplier_registrations

      root to: "users#index"
    end
      devise_for :users, controllers: {
        sessions: 'users/sessions'
      }
 
  resources :supplier_registrations
  resources :orders
  resources :operations_teams
  resources :homes
  resources :enquiry_details_pages
  resources :enquiries
  resources :customisations
  resources :client_registrations
  root to:"homes#index"

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
# == Route Map
#
#                           Prefix Verb   URI Pattern                                                                              Controller#Action
#                      admin_users GET    /admin/users(.:format)                                                                   admin/users#index
#                                  POST   /admin/users(.:format)                                                                   admin/users#create
#                   new_admin_user GET    /admin/users/new(.:format)                                                               admin/users#new
#                  edit_admin_user GET    /admin/users/:id/edit(.:format)                                                          admin/users#edit
#                       admin_user GET    /admin/users/:id(.:format)                                                               admin/users#show
#                                  PATCH  /admin/users/:id(.:format)                                                               admin/users#update
#                                  PUT    /admin/users/:id(.:format)                                                               admin/users#update
#                                  DELETE /admin/users/:id(.:format)                                                               admin/users#destroy
#       admin_client_registrations GET    /admin/client_registrations(.:format)                                                    admin/client_registrations#index
#                                  POST   /admin/client_registrations(.:format)                                                    admin/client_registrations#create
#    new_admin_client_registration GET    /admin/client_registrations/new(.:format)                                                admin/client_registrations#new
#   edit_admin_client_registration GET    /admin/client_registrations/:id/edit(.:format)                                           admin/client_registrations#edit
#        admin_client_registration GET    /admin/client_registrations/:id(.:format)                                                admin/client_registrations#show
#                                  PATCH  /admin/client_registrations/:id(.:format)                                                admin/client_registrations#update
#                                  PUT    /admin/client_registrations/:id(.:format)                                                admin/client_registrations#update
#                                  DELETE /admin/client_registrations/:id(.:format)                                                admin/client_registrations#destroy
#             admin_customisations GET    /admin/customisations(.:format)                                                          admin/customisations#index
#                                  POST   /admin/customisations(.:format)                                                          admin/customisations#create
#          new_admin_customisation GET    /admin/customisations/new(.:format)                                                      admin/customisations#new
#         edit_admin_customisation GET    /admin/customisations/:id/edit(.:format)                                                 admin/customisations#edit
#              admin_customisation GET    /admin/customisations/:id(.:format)                                                      admin/customisations#show
#                                  PATCH  /admin/customisations/:id(.:format)                                                      admin/customisations#update
#                                  PUT    /admin/customisations/:id(.:format)                                                      admin/customisations#update
#                                  DELETE /admin/customisations/:id(.:format)                                                      admin/customisations#destroy
#                  admin_enquiries GET    /admin/enquiries(.:format)                                                               admin/enquiries#index
#                                  POST   /admin/enquiries(.:format)                                                               admin/enquiries#create
#                new_admin_enquiry GET    /admin/enquiries/new(.:format)                                                           admin/enquiries#new
#               edit_admin_enquiry GET    /admin/enquiries/:id/edit(.:format)                                                      admin/enquiries#edit
#                    admin_enquiry GET    /admin/enquiries/:id(.:format)                                                           admin/enquiries#show
#                                  PATCH  /admin/enquiries/:id(.:format)                                                           admin/enquiries#update
#                                  PUT    /admin/enquiries/:id(.:format)                                                           admin/enquiries#update
#                                  DELETE /admin/enquiries/:id(.:format)                                                           admin/enquiries#destroy
#      admin_enquiry_details_pages GET    /admin/enquiry_details_pages(.:format)                                                   admin/enquiry_details_pages#index
#                                  POST   /admin/enquiry_details_pages(.:format)                                                   admin/enquiry_details_pages#create
#   new_admin_enquiry_details_page GET    /admin/enquiry_details_pages/new(.:format)                                               admin/enquiry_details_pages#new
#  edit_admin_enquiry_details_page GET    /admin/enquiry_details_pages/:id/edit(.:format)                                          admin/enquiry_details_pages#edit
#       admin_enquiry_details_page GET    /admin/enquiry_details_pages/:id(.:format)                                               admin/enquiry_details_pages#show
#                                  PATCH  /admin/enquiry_details_pages/:id(.:format)                                               admin/enquiry_details_pages#update
#                                  PUT    /admin/enquiry_details_pages/:id(.:format)                                               admin/enquiry_details_pages#update
#                                  DELETE /admin/enquiry_details_pages/:id(.:format)                                               admin/enquiry_details_pages#destroy
#                      admin_homes GET    /admin/homes(.:format)                                                                   admin/homes#index
#                                  POST   /admin/homes(.:format)                                                                   admin/homes#create
#                   new_admin_home GET    /admin/homes/new(.:format)                                                               admin/homes#new
#                  edit_admin_home GET    /admin/homes/:id/edit(.:format)                                                          admin/homes#edit
#                       admin_home GET    /admin/homes/:id(.:format)                                                               admin/homes#show
#                                  PATCH  /admin/homes/:id(.:format)                                                               admin/homes#update
#                                  PUT    /admin/homes/:id(.:format)                                                               admin/homes#update
#                                  DELETE /admin/homes/:id(.:format)                                                               admin/homes#destroy
#           admin_operations_teams GET    /admin/operations_teams(.:format)                                                        admin/operations_teams#index
#                                  POST   /admin/operations_teams(.:format)                                                        admin/operations_teams#create
#        new_admin_operations_team GET    /admin/operations_teams/new(.:format)                                                    admin/operations_teams#new
#       edit_admin_operations_team GET    /admin/operations_teams/:id/edit(.:format)                                               admin/operations_teams#edit
#            admin_operations_team GET    /admin/operations_teams/:id(.:format)                                                    admin/operations_teams#show
#                                  PATCH  /admin/operations_teams/:id(.:format)                                                    admin/operations_teams#update
#                                  PUT    /admin/operations_teams/:id(.:format)                                                    admin/operations_teams#update
#                                  DELETE /admin/operations_teams/:id(.:format)                                                    admin/operations_teams#destroy
#                     admin_orders GET    /admin/orders(.:format)                                                                  admin/orders#index
#                                  POST   /admin/orders(.:format)                                                                  admin/orders#create
#                  new_admin_order GET    /admin/orders/new(.:format)                                                              admin/orders#new
#                 edit_admin_order GET    /admin/orders/:id/edit(.:format)                                                         admin/orders#edit
#                      admin_order GET    /admin/orders/:id(.:format)                                                              admin/orders#show
#                                  PATCH  /admin/orders/:id(.:format)                                                              admin/orders#update
#                                  PUT    /admin/orders/:id(.:format)                                                              admin/orders#update
#                                  DELETE /admin/orders/:id(.:format)                                                              admin/orders#destroy
#     admin_supplier_registrations GET    /admin/supplier_registrations(.:format)                                                  admin/supplier_registrations#index
#                                  POST   /admin/supplier_registrations(.:format)                                                  admin/supplier_registrations#create
#  new_admin_supplier_registration GET    /admin/supplier_registrations/new(.:format)                                              admin/supplier_registrations#new
# edit_admin_supplier_registration GET    /admin/supplier_registrations/:id/edit(.:format)                                         admin/supplier_registrations#edit
#      admin_supplier_registration GET    /admin/supplier_registrations/:id(.:format)                                              admin/supplier_registrations#show
#                                  PATCH  /admin/supplier_registrations/:id(.:format)                                              admin/supplier_registrations#update
#                                  PUT    /admin/supplier_registrations/:id(.:format)                                              admin/supplier_registrations#update
#                                  DELETE /admin/supplier_registrations/:id(.:format)                                              admin/supplier_registrations#destroy
#                       admin_root GET    /admin(.:format)                                                                         admin/users#index
#                 new_user_session GET    /users/sign_in(.:format)                                                                 users/sessions#new
#                     user_session POST   /users/sign_in(.:format)                                                                 users/sessions#create
#             destroy_user_session DELETE /users/sign_out(.:format)                                                                users/sessions#destroy
#                new_user_password GET    /users/password/new(.:format)                                                            devise/passwords#new
#               edit_user_password GET    /users/password/edit(.:format)                                                           devise/passwords#edit
#                    user_password PATCH  /users/password(.:format)                                                                devise/passwords#update
#                                  PUT    /users/password(.:format)                                                                devise/passwords#update
#                                  POST   /users/password(.:format)                                                                devise/passwords#create
#         cancel_user_registration GET    /users/cancel(.:format)                                                                  devise/registrations#cancel
#            new_user_registration GET    /users/sign_up(.:format)                                                                 devise/registrations#new
#           edit_user_registration GET    /users/edit(.:format)                                                                    devise/registrations#edit
#                user_registration PATCH  /users(.:format)                                                                         devise/registrations#update
#                                  PUT    /users(.:format)                                                                         devise/registrations#update
#                                  DELETE /users(.:format)                                                                         devise/registrations#destroy
#                                  POST   /users(.:format)                                                                         devise/registrations#create
#           supplier_registrations GET    /supplier_registrations(.:format)                                                        supplier_registrations#index
#                                  POST   /supplier_registrations(.:format)                                                        supplier_registrations#create
#        new_supplier_registration GET    /supplier_registrations/new(.:format)                                                    supplier_registrations#new
#       edit_supplier_registration GET    /supplier_registrations/:id/edit(.:format)                                               supplier_registrations#edit
#            supplier_registration GET    /supplier_registrations/:id(.:format)                                                    supplier_registrations#show
#                                  PATCH  /supplier_registrations/:id(.:format)                                                    supplier_registrations#update
#                                  PUT    /supplier_registrations/:id(.:format)                                                    supplier_registrations#update
#                                  DELETE /supplier_registrations/:id(.:format)                                                    supplier_registrations#destroy
#                           orders GET    /orders(.:format)                                                                        orders#index
#                                  POST   /orders(.:format)                                                                        orders#create
#                        new_order GET    /orders/new(.:format)                                                                    orders#new
#                       edit_order GET    /orders/:id/edit(.:format)                                                               orders#edit
#                            order GET    /orders/:id(.:format)                                                                    orders#show
#                                  PATCH  /orders/:id(.:format)                                                                    orders#update
#                                  PUT    /orders/:id(.:format)                                                                    orders#update
#                                  DELETE /orders/:id(.:format)                                                                    orders#destroy
#                 operations_teams GET    /operations_teams(.:format)                                                              operations_teams#index
#                                  POST   /operations_teams(.:format)                                                              operations_teams#create
#              new_operations_team GET    /operations_teams/new(.:format)                                                          operations_teams#new
#             edit_operations_team GET    /operations_teams/:id/edit(.:format)                                                     operations_teams#edit
#                  operations_team GET    /operations_teams/:id(.:format)                                                          operations_teams#show
#                                  PATCH  /operations_teams/:id(.:format)                                                          operations_teams#update
#                                  PUT    /operations_teams/:id(.:format)                                                          operations_teams#update
#                                  DELETE /operations_teams/:id(.:format)                                                          operations_teams#destroy
#                            homes GET    /homes(.:format)                                                                         homes#index
#                                  POST   /homes(.:format)                                                                         homes#create
#                         new_home GET    /homes/new(.:format)                                                                     homes#new
#                        edit_home GET    /homes/:id/edit(.:format)                                                                homes#edit
#                             home GET    /homes/:id(.:format)                                                                     homes#show
#                                  PATCH  /homes/:id(.:format)                                                                     homes#update
#                                  PUT    /homes/:id(.:format)                                                                     homes#update
#                                  DELETE /homes/:id(.:format)                                                                     homes#destroy
#            enquiry_details_pages GET    /enquiry_details_pages(.:format)                                                         enquiry_details_pages#index
#                                  POST   /enquiry_details_pages(.:format)                                                         enquiry_details_pages#create
#         new_enquiry_details_page GET    /enquiry_details_pages/new(.:format)                                                     enquiry_details_pages#new
#        edit_enquiry_details_page GET    /enquiry_details_pages/:id/edit(.:format)                                                enquiry_details_pages#edit
#             enquiry_details_page GET    /enquiry_details_pages/:id(.:format)                                                     enquiry_details_pages#show
#                                  PATCH  /enquiry_details_pages/:id(.:format)                                                     enquiry_details_pages#update
#                                  PUT    /enquiry_details_pages/:id(.:format)                                                     enquiry_details_pages#update
#                                  DELETE /enquiry_details_pages/:id(.:format)                                                     enquiry_details_pages#destroy
#                        enquiries GET    /enquiries(.:format)                                                                     enquiries#index
#                                  POST   /enquiries(.:format)                                                                     enquiries#create
#                      new_enquiry GET    /enquiries/new(.:format)                                                                 enquiries#new
#                     edit_enquiry GET    /enquiries/:id/edit(.:format)                                                            enquiries#edit
#                          enquiry GET    /enquiries/:id(.:format)                                                                 enquiries#show
#                                  PATCH  /enquiries/:id(.:format)                                                                 enquiries#update
#                                  PUT    /enquiries/:id(.:format)                                                                 enquiries#update
#                                  DELETE /enquiries/:id(.:format)                                                                 enquiries#destroy
#                   customisations GET    /customisations(.:format)                                                                customisations#index
#                                  POST   /customisations(.:format)                                                                customisations#create
#                new_customisation GET    /customisations/new(.:format)                                                            customisations#new
#               edit_customisation GET    /customisations/:id/edit(.:format)                                                       customisations#edit
#                    customisation GET    /customisations/:id(.:format)                                                            customisations#show
#                                  PATCH  /customisations/:id(.:format)                                                            customisations#update
#                                  PUT    /customisations/:id(.:format)                                                            customisations#update
#                                  DELETE /customisations/:id(.:format)                                                            customisations#destroy
#             client_registrations GET    /client_registrations(.:format)                                                          client_registrations#index
#                                  POST   /client_registrations(.:format)                                                          client_registrations#create
#          new_client_registration GET    /client_registrations/new(.:format)                                                      client_registrations#new
#         edit_client_registration GET    /client_registrations/:id/edit(.:format)                                                 client_registrations#edit
#              client_registration GET    /client_registrations/:id(.:format)                                                      client_registrations#show
#                                  PATCH  /client_registrations/:id(.:format)                                                      client_registrations#update
#                                  PUT    /client_registrations/:id(.:format)                                                      client_registrations#update
#                                  DELETE /client_registrations/:id(.:format)                                                      client_registrations#destroy
#                             root GET    /                                                                                        homes#index
#               rails_service_blob GET    /rails/active_storage/blobs/:signed_id/*filename(.:format)                               active_storage/blobs#show
#        rails_blob_representation GET    /rails/active_storage/representations/:signed_blob_id/:variation_key/*filename(.:format) active_storage/representations#show
#               rails_disk_service GET    /rails/active_storage/disk/:encoded_key/*filename(.:format)                              active_storage/disk#show
#        update_rails_disk_service PUT    /rails/active_storage/disk/:encoded_token(.:format)                                      active_storage/disk#update
#             rails_direct_uploads POST   /rails/active_storage/direct_uploads(.:format)                                           active_storage/direct_uploads#create
