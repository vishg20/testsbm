require "administrate/base_dashboard"

class EnquiryDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    client_name: Field::String,
    enquiry_for: Field::String,
    color: Field::String,
    material: Field::String,
    gsm: Field::String,
    quantity: Field::Number,
    sizes: Field::String,
    printing: Field::String,
    embroidery: Field::String,
    place_of_printing: Field::String,
    payment_terms: Field::String,
    client_target_price: Field::String,
    client_target_delivery_schedule: Field::String,
    owner_of_the_etask: Field::String,
    status_of_the_enquiry: Field::String,
    tenant_id: Field::String,
    client_id: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :client_name,
    :enquiry_for,
    :color,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :client_name,
    :enquiry_for,
    :color,
    :material,
    :gsm,
    :quantity,
    :sizes,
    :printing,
    :embroidery,
    :place_of_printing,
    :payment_terms,
    :client_target_price,
    :client_target_delivery_schedule,
    :owner_of_the_etask,
    :status_of_the_enquiry,
    :tenant_id,
    :client_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :client_name,
    :enquiry_for,
    :color,
    :material,
    :gsm,
    :quantity,
    :sizes,
    :printing,
    :embroidery,
    :place_of_printing,
    :payment_terms,
    :client_target_price,
    :client_target_delivery_schedule,
    :owner_of_the_etask,
    :status_of_the_enquiry,
    :tenant_id,
    :client_id,
  ].freeze

  # Overwrite this method to customize how enquiries are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(enquiry)
  #   "Enquiry ##{enquiry.id}"
  # end
end
