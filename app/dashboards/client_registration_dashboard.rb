require "administrate/base_dashboard"

class ClientRegistrationDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    id: Field::Number,
    company_name: Field::String,
    buyer_name: Field::String,
    phone_number: Field::String,
    email_id: Field::String,
    mobile_number: Field::Number,
    purchasing_manager_name: Field::String,
    address: Field::String,
    nature_of_business: Field::String,
    login_id: Field::String,
    tenant_id: Field::String,
    client_registration_id: Field::String,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = [
    :id,
    :company_name,
    :buyer_name,
    :phone_number,
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = [
    :id,
    :company_name,
    :buyer_name,
    :phone_number,
    :email_id,
    :mobile_number,
    :purchasing_manager_name,
    :address,
    :nature_of_business,
    :login_id,
    :tenant_id,
    :client_registration_id,
    :created_at,
    :updated_at,
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = [
    :company_name,
    :buyer_name,
    :phone_number,
    :email_id,
    :mobile_number,
    :purchasing_manager_name,
    :address,
    :nature_of_business,
    :login_id,
    :tenant_id,
    :client_registration_id,
  ].freeze

  # Overwrite this method to customize how client registrations are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(client_registration)
  #   "ClientRegistration ##{client_registration.id}"
  # end
end
