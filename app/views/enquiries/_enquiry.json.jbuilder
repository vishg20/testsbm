json.extract! enquiry, :id, :client_name, :enquiry_for, :color, :material, :gsm, :quantity, :sizes, :printing, :embroidery, :place_of_printing, :payment_terms, :client_target_price, :client_target_delivery_schedule, :owner_of_the_etask, :status_of_the_enquiry, :tenant_id, :client_id, :created_at, :updated_at
json.url enquiry_url(enquiry, format: :json)
