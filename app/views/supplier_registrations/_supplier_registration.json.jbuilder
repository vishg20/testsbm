json.extract! supplier_registration, :id, :company_name, :supplier_name, :phone_number, :email_id, :mobile_number, :manager_name, :address, :nature_of_business, :login_id, :tenant_id, :created_at, :updated_at
json.url supplier_registration_url(supplier_registration, format: :json)
