json.extract! customisation, :id, :type, :place_of_printing, :place_of_embroidery, :tenant_id, :created_at, :updated_at
json.url customisation_url(customisation, format: :json)
