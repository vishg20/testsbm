class OrdersController < ApplicationController
  before_action :authenticate_user!
  before_action :set_order, only: [:show, :update, :destroy]
  after_action :verify_authorized
  # GET /orders
  # GET /orders.json
  def index
    @orders = Order.all
    authorize @orders
    @orders = policy_scope(Order)
  end

  # GET /orders/1
  # GET /orders/1.json
  def show
    authorize @order
  end
  # GET /orders/new
  def new
    @order = Order.new
    authorize @order

  end
  # GET /orders/1/edit
  def edit
    authorize @order
  end
  # POST /orders
  # POST /orders.json
  def create
    @order = Order.new(order_params.merge(user_id: current_user.id))
    authorize @order
    if @order.save
      render :show, status: :created, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /orders/1
  # PATCH/PUT /orders/1.json
  def update
    authorize @order
    if @order.update(order_params)
      render :show, status: :ok, location: @order
    else
      render json: @order.errors, status: :unprocessable_entity
    end
  end

  # DELETE /orders/1
  # DELETE /orders/1.json
  def destroy
    authorize @order
    @order.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Order.find(params[:id])
      authorize @order
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:customer_name, :enquiry_number, :client_name, :enquiry_for, :color, :material, :gsm, :quantity, :sizes, :printing, :embroidery, :place_of_printing, :payment_terms, :client_target_price, :client_target_delivery_schedule, :offered_price, :offered_delivery_schedule, :status_of_the_enquiry, :supplier_name, :sample_readiness, :sample__delivery, :advance_received, :status_of_the_order, :owner_of_the_otask, :tenant_id, :user_id)
    end
end
