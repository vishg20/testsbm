class OperationsTeamsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_operations_team, only: [:show, :update, :destroy]
  after_action :verify_authorized
  # GET /operations_teams
  # GET /operations_teams.json
  def index
    @operations_teams = OperationsTeam.all
    authorize @operations_teams
    @operations_teams = policy_scope(OperationsTeam)
  end

  # GET /operations_teams/1
  # GET /operations_teams/1.json
  def show
    authorize @operations_team
  end
    # GET /operations_teams/new
  def new
    @operations_team = OperationsTeam.new
    authorize @operations_team
     
  end
    # GET /operations_teams/1/edit
  def edit
    authorize @operations_team
  end
  # POST /operations_teams
  # POST /operations_teams.json
  def create
    @operations_team = OperationsTeam.new(operations_team_params.merge(user_id: current_user.id))
    authorize @operations_team

    if @operations_team.save
      render :show, status: :created, location: @operations_team
    else
      render json: @operations_team.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /operations_teams/1
  # PATCH/PUT /operations_teams/1.json
  def update
    authorize @operations_team
    if @operations_team.update(operations_team_params)
      render :show, status: :ok, location: @operations_team
    else
      render json: @operations_team.errors, status: :unprocessable_entity
    end
  end

  # DELETE /operations_teams/1
  # DELETE /operations_teams/1.json
  def destroy
    authorize @operations_team
    @operations_team.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_operations_team
      @operations_team = OperationsTeam.find(params[:id])
      authorize @operations_team
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def operations_team_params
      params.require(:operations_team).permit(:name, :mobile_number, :email_id, :login_id, :tenant_id, :user_id)
    end
end
