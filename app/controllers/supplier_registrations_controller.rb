class SupplierRegistrationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_supplier_registration, only: [:show, :update, :destroy]
  after_action :verify_authorized
  # GET /supplier_registrations
  # GET /supplier_registrations.json
  def index
    @supplier_registrations = SupplierRegistration.all
    authorize @supplier_registrations
    @supplier_registrations = policy_scope(SupplierRegistration)
  end

  # GET /supplier_registrations/1
  # GET /supplier_registrations/1.json
  def show
    authorize @supplier_registration
  end
  # GET /supplier_registrations/new
  def new
    @supplier_registration = SupplierRegistration.new
    authorize @supplier_registration
  
  end
  # GET /supplier_registrations/1/edit
  def edit
    authorize @supplier_registration
  end
  # POST /supplier_registrations
  # POST /supplier_registrations.json
  def create
    @supplier_registration = SupplierRegistration.new(supplier_registration_params.merge(user_id: current_user.id))
    authorize @supplier_registration

    if @supplier_registration.save
      render :show, status: :created, location: @supplier_registration
    else
      render json: @supplier_registration.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /supplier_registrations/1
  # PATCH/PUT /supplier_registrations/1.json
  def update
    authorize @supplier_registration
    if @supplier_registration.update(supplier_registration_params)
      render :show, status: :ok, location: @supplier_registration
    else
      render json: @supplier_registration.errors, status: :unprocessable_entity
    end
  end

  # DELETE /supplier_registrations/1
  # DELETE /supplier_registrations/1.json
  def destroy
    authorize @supplier_registration
    @supplier_registration.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_supplier_registration
      @supplier_registration = SupplierRegistration.find(params[:id])
      authorize @supplier_registration
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def supplier_registration_params
      params.require(:supplier_registration).permit(:company_name, :supplier_name, :phone_number, :email_id, :mobile_number, :manager_name, :address, :nature_of_business, :login_id, :tenant_id, :user_id)
    end
end
