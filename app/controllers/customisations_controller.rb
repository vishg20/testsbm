class CustomisationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_customisation, only: [:show, :update, :destroy]
  after_action :verify_authorized
  # GET /customisations
  # GET /customisations.json
  def index
    @customisations = Customisation.all
    authorize @customisations
    @customisations = policy_scope(Customisation)
  end

  # GET /customisations/1
  # GET /customisations/1.json
  def show
    authorize @customisation
  end
   # GET /customisations/new
  def new
    @customisation = Customisation.new
    authorize @customisation
   end
  # GET /customisations/1/edit
  def edit
    authorize @customisation
  end
  # POST /customisations
  # POST /customisations.json
  def create
    @customisation = Customisation.new(customisation_params.merge(user_id: current_user.id))
    authorize @customisation
    if @customisation.save
      render :show, status: :created, location: @customisation
    else
      render json: @customisation.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /customisations/1
  # PATCH/PUT /customisations/1.json
  def update
    authorize @customisation
    if @customisation.update(customisation_params)
      render :show, status: :ok, location: @customisation
    else
      render json: @customisation.errors, status: :unprocessable_entity
    end
  end

  # DELETE /customisations/1
  # DELETE /customisations/1.json
  def destroy
    authorize @customisation
    @customisation.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_customisation
      @customisation = Customisation.find(params[:id])
      authorize @customisation
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def customisation_params
      params.require(:customisation).permit(:type, :place_of_printing, :place_of_embroidery, :tenant_id, :user_id)
    end
end
