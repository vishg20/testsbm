class HomesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_home, only: [:show, :update, :destroy]
  after_action :verify_authorized
  skip_before_action :verify_authenticity_token 
  # GET /homes
  # GET /homes.json
  def index
    @homes = Home.all
    authorize @homes
  end

  # GET /homes/1
  # GET /homes/1.json
  def show
    authorize @home
  end
   # GET /homes/new
  def new
    @home = Home.new
    authorize @home
    
  end
  # GET /homes/1/edit
  def edit
    authorize @home
  end

  # POST /homes
  # POST /homes.json
  def create
    @home = Home.new(home_params)
    authorize @home

    if @home.save
      render :show, status: :created, location: @home
    else
      render json: @home.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /homes/1
  # PATCH/PUT /homes/1.json
  def update
    authorize @home
    if @home.update(home_params)
      render :show, status: :ok, location: @home
    else
      render json: @home.errors, status: :unprocessable_entity
    end
  end

  # DELETE /homes/1
  # DELETE /homes/1.json
  def destroy
    authorize @home
    @home.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_home
      @home = Home.find(params[:id])
      authorize @home
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def home_params
      params.fetch(:home, {})
    end
end
