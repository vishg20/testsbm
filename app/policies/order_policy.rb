# frozen_string_literal: true

class OrderPolicy < ApplicationPolicy
  def index?
    true
  end

  def new?
    true
   end

  def create?
    user.present?
   end

  def update?
    return true if user.present? && user == order.user

    user.present? && user == order.user
   end

  def destroy?
    user.present? && user.admin?
   end
class Scope
    attr_reader :user, :scope

    def initialize(user, scope)
      @user = user
      @scope = scope
    end

    def resolve
     
      if user.admin?
        scope.all
      else
        scope.where(user_id: user)
     end
    end
    end

  private

  def order
    record
   end
end
