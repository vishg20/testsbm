class SupplierRegistration < ApplicationRecord
  belongs_to :user, optional: true
  has_many :orders
end
# == Schema Information
#
# Table name: supplier_registrations
#
#  id                 :bigint           not null, primary key
#  company_name       :string(255)
#  supplier_name      :string(255)
#  phone_number       :string(255)
#  email_id           :string(255)
#  mobile_number      :string(255)
#  manager_name       :string(255)
#  address            :string(255)
#  nature_of_business :string(255)
#  login_id           :string(255)
#  tenant_id          :string(255)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#