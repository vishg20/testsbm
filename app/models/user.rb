class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  has_many :enquiries
  has_one :client_registration
  has_one :supplier_registration
  has_one :operations_team
  has_one :roles
  def admin?
    role == 'admin'
  end

  def client?
    role == 'client'
  end

  def supplier?
    role == 'supplier'
  end

  def operations_team?
    role == 'operations_team'
  end
end
# == Schema Information
#
# Table name: users
#
#  id                     :bigint           not null, primary key
#  email                  :string(255)      default(""), not null
#  encrypted_password     :string(255)      default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  role                   :string(255)

