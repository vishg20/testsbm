class Order < ApplicationRecord
  belongs_to :enquiry
  belongs_to :user, optional: true
  belongs_to :client
  has_one :supplier	
end
# == Schema Information
#
# Table name: orders
#
#  id                              :bigint           not null, primary key
#  customer_name                   :string(255)
#  enquiry_number                  :string(255)
#  client_name                     :string(255)
#  enquiry_for                     :string(255)
#  color                           :string(255)
#  material                        :string(255)
#  gsm                             :string(255)
#  quantity                        :integer
#  sizes                           :string(255)
#  printing                        :string(255)
#  embroidery                      :string(255)
#  place_of_printing               :string(255)
#  payment_terms                   :string(255)
#  client_target_price             :string(255)
#  client_target_delivery_schedule :string(255)
#  offered_price                   :string(255)
#  offered_delivery_schedule       :string(255)
#  status_of_the_enquiry           :string(255)
#  supplier_name                   :string(255)
#  sample_readiness                :string(255)
#  sample__delivery                :string(255)
#  advance_received                :string(255)
#  status_of_the_order             :string(255)
#  owner_of_the_otask              :string(255)
#  tenant_id                       :string(255)
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#