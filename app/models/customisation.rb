class Customisation < ApplicationRecord
	belongs_to :enquiry
end
# == Schema Information
#
# Table name: customisations
#
#  id                  :bigint           not null, primary key
#  type                :string(255)
#  place_of_printing   :string(255)
#  place_of_embroidery :string(255)
#  tenant_id           :string(255)
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#