class ClientRegistration < ApplicationRecord
 # has_many :enquiries
 # has_many :orders
  belongs_to :user, optional: true
end
# == Schema Information
#
# Table name: client_registrations
#
#  id                      :bigint           not null, primary key
#  company_name            :string(255)
#  buyer_name              :string(255)
#  phone_number            :string(255)
#  email_id                :string(255)
#  mobile_number           :integer
#  purchasing_manager_name :string(255)
#  address                 :string(255)
#  nature_of_business      :string(255)
#  login_id                :string(255)
#  tenant_id               :string(255)
#  client_registration_id  :string(255)
#  created_at              :datetime         not null
#  updated_at              :datetime         not null
#