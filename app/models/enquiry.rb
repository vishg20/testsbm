class Enquiry < ApplicationRecord
   has_one :enquiry_details_page
   #has_one :order
   belongs_to :user, optional: true,foreign_key: true
   has_many :customisations
  # belongs_to :client

end
# == Schema Information
#
# Table name: enquiries
#
#  id                              :bigint           not null, primary key
#  client_name                     :string(255)
#  enquiry_for                     :string(255)
#  color                           :string(255)
#  material                        :string(255)
#  gsm                             :string(255)
#  quantity                        :integer
#  sizes                           :string(255)
#  printing                        :string(255)
#  embroidery                      :string(255)
#  place_of_printing               :string(255)
#  payment_terms                   :string(255)
#  client_target_price             :string(255)
#  client_target_delivery_schedule :string(255)
#  owner_of_the_etask              :string(255)
#  status_of_the_enquiry           :string(255)
#  tenant_id                       :string(255)
#  client_id                       :string(255)
#  created_at                      :datetime         not null
#  updated_at                      :datetime         not null
#
