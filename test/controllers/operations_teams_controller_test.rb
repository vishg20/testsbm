require 'test_helper'

class OperationsTeamsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @operations_team = operations_teams(:one)
  end

  test "should get index" do
    get operations_teams_url, as: :json
    assert_response :success
  end

  test "should create operations_team" do
    assert_difference('OperationsTeam.count') do
      post operations_teams_url, params: { operations_team: { email_id: @operations_team.email_id, login_id: @operations_team.login_id, mobile_number: @operations_team.mobile_number, name: @operations_team.name, tenant_id: @operations_team.tenant_id } }, as: :json
    end

    assert_response 201
  end

  test "should show operations_team" do
    get operations_team_url(@operations_team), as: :json
    assert_response :success
  end

  test "should update operations_team" do
    patch operations_team_url(@operations_team), params: { operations_team: { email_id: @operations_team.email_id, login_id: @operations_team.login_id, mobile_number: @operations_team.mobile_number, name: @operations_team.name, tenant_id: @operations_team.tenant_id } }, as: :json
    assert_response 200
  end

  test "should destroy operations_team" do
    assert_difference('OperationsTeam.count', -1) do
      delete operations_team_url(@operations_team), as: :json
    end

    assert_response 204
  end
end
