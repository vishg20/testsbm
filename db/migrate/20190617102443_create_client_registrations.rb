class CreateClientRegistrations < ActiveRecord::Migration[5.2]
  def change
    create_table :client_registrations do |t|
      t.string :company_name
      t.string :buyer_name
      t.string :phone_number
      t.string :email_id
      t.integer :mobile_number
      t.string :purchasing_manager_name
      t.string :address
      t.string :nature_of_business
      t.string :login_id
      t.string :tenant_id
      t.string :client_registration_id

      t.timestamps
    end
  end
end
