class AddUserRefToClientRegistrations < ActiveRecord::Migration[5.2]
  def change
    add_reference :client_registrations, :user, foreign_key: true
  end
end
