class AddUserRefToSupplierRegistrations < ActiveRecord::Migration[5.2]
  def change
    add_reference :supplier_registrations, :user, foreign_key: true
  end
end
