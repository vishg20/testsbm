class CreateCustomisations < ActiveRecord::Migration[5.2]
  def change
    create_table :customisations do |t|
      t.string :type
      t.string :place_of_printing
      t.string :place_of_embroidery
      t.string :tenant_id

      t.timestamps
    end
  end
end
