class CreateEnquiries < ActiveRecord::Migration[5.2]
  def change
    create_table :enquiries do |t|
      t.string :client_name
      t.string :enquiry_for
      t.string :color
      t.string :material
      t.string :gsm
      t.integer :quantity
      t.string :sizes
      t.string :printing
      t.string :embroidery
      t.string :place_of_printing
      t.string :payment_terms
      t.string :client_target_price
      t.string :client_target_delivery_schedule
      t.string :owner_of_the_etask
      t.string :status_of_the_enquiry
      t.string :tenant_id
      t.string :client_id

      t.timestamps
    end
  end
end
