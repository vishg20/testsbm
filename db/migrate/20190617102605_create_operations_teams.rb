class CreateOperationsTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :operations_teams do |t|
      t.string :name
      t.string :mobile_number
      t.string :email_id
      t.string :login_id
      t.string :tenant_id

      t.timestamps
    end
  end
end
